var app = angular.module('MyApp', []);

app.directive('myTable', function(){
	return {
		scope: {
			name: '@',
			page: '='
		}, 
		controller: function($scope, $element, $attrs, $transclude, $http, $timeout) {			
			$scope.update_local_storage = function () {
				var data = JSON.stringify($scope.table);
				data = data.replace(/,"\$\$hashKey":"object:[0-9]{0,}"/gi, '');
				localStorage.setItem('table_' + $scope.table.name, data);
			}
			
			$scope.link_and_check_table = function (argument) {
				$scope.page.tables[$scope.name].data_link = $scope.table;
				$scope.page.ready_tables ++;
			}
			if (!localStorage.getItem('table_' + $scope.name)){
				$http({
					method: 'GET',
					url: 'js/json/' + $scope.name + '_table.json'
				}).then(
					function sucessCallback (response) {
						$scope.table = new Object();
						$scope.table.name = response.data.name;
						$scope.table.columns = response.data.columns;
						$http({
					  		method: 'GET',
					  		url: 'js/json/' + $scope.table.name + '.json',
						}).then(function successCallback(response) {
					  		$scope.table.data = response.data;
					  	}).then(function () {
					  		$scope.update_local_storage();
							$scope.link_and_check_table();
					  	})
				})
			} else {
				$scope.table = JSON.parse(localStorage.getItem('table_' + $scope.name));
				$scope.link_and_check_table();

				// $.each($scope.table.columns, function(index, val) {
				// 	if (val.ref_key === 'YES'){
				// 		var column_for_change_name = val.name;
				// 		var ref_table = val.ref_table;
				// 		var column_in_ref_table = val.ref_row;
				// 		$.each($scope.table.data, function (index, val) {
				// 			$.each(val, function (index, val) {
				// 				if (index === column_for_change_name){
				// 					console.log(index, val);
				// 					var key_in_ref_table = val;
				// 					var new_val = $scope.page.tables[ref_table]
				// 					// [column_in_ref_table][key_in_ref_table];
				// 					console.log(new_val);
				// 				}
				// 			})
				// 		})
				// 	}
				// });
			}
		
			$scope.activator = true;
			$scope.show_shadow = true;
			$scope.table_class = 'little-block';
			
			$scope.choosen = function () {
				$scope.show_shadow = false;
				$scope.table_class = '';
				$scope.page.active = $scope.name;
			}
			$scope.back = function () {
				$scope.show_shadow = true;
				$scope.table_class = 'little-block';
				$scope.page.active = null;
			}
			
			$scope.$watch(function () {
				return $scope.page.active;
			}, function (newValue, oldValue) {
				if (newValue === oldValue) return;
				if ((newValue == null) || (newValue == $scope.name)){
					$scope.activator = true;
				} else {
					$scope.activator = false;
				}
			}, true)

			$scope.add_item = function () {
				var forms = $element.find('form');
				angular.forEach(forms, function (item, key) {
					if (item.id !== $scope.name){
						return
					} else{
						new_data = item
					}
				})
				new_data = new_data.getElementsByTagName('input');
				var new_user = {};
				for (var i = 0; i < new_data.length; i++) {
					new_user[new_data[i].getAttribute('field')] = new_data[i].value;
					new_data[i].value = '';
				};
				
		        $scope.page.notificator('Ваша запись обрабатывается сервером...', 'info')
				$timeout(function () {
					$scope.table.data.push(new_user);
		        	$scope.page.notificator('Запись успешно добавлена', 'success')

		        	$scope.update_local_storage();
				}, 1000);
			}
			$scope.delete = function (target) {
				$scope.page.notificator("Ваша запись обрабатывается сервером...", 'info')
				$timeout(function () {
					$scope.table.data.splice(target, 1)
					$scope.page.notificator("Запись успешно удалена", 'success');

		        	$scope.update_local_storage();
				}, 1000);
			}
			$scope.change = function (target) {
				$scope.change_id = target;
				$scope.form_change_person = true;
				$scope.form_add_person = false;
			}
			$scope.apply_change_item = function () {
				var forms = $element.find('form');
				angular.forEach(forms, function (item, key) {
					if (item.id !== 'change'){
						return
					} else{
						new_data = item
					}
				})
				new_data = new_data.getElementsByTagName('input');
				
				var new_user = {};
				for (var i = 0; i < new_data.length; i++) {
					new_user[new_data[i].getAttribute('field')] = new_data[i].value;
					// new_data[i].value = '';
				};
				$scope.page.notificator("Ваша запись обрабатывается сервером...", 'info')
				//new_data ready for send to server
				$timeout(function () {
					$scope.table.data[$scope.change_id] = new_user;
					$scope.form_change_person = false;
					$scope.page.notificator("Запись успешно добавлена", 'success');
					$scope.update_local_storage();
				}, 1000);
			}
			$scope.read_full = function (key) {
				$.each($scope.table.columns, function(index, val) {
					if (val.ref_key === 'YES'){
						var column_for_change_name = val.name;
						var ref_table = val.ref_table;
						var column_in_ref_table = val.ref_row;

						$.each($scope.table.data[key], function (index, val) {
							if (index === column_for_change_name){
								var key_in_ref_table = val;
								var new_row = $scope.page.tables[ref_table].data_link;
								var buf = [];
								$.each($scope.page.tables[ref_table].data_link.data, function (index, val) {
									
									if (val[column_in_ref_table] == key_in_ref_table){
										var buf = '';
										$.each(val, function (index, val) {
											// console.log(index, val);
											if (index!= '$$hashKey'){		
												buf += index + ' : ' + val + '\n';
											}
										})

										$scope.page.notificator(buf, 'info', 50000)
									}
								})
							}
						})
						
					}
				})
			}

		},
		restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
		templateUrl: 'js/tmpl/MyTable.html',
		replace: true,
		// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
		link: function($scope, iElm, iAttrs, controller) {
		}
	};
});

app.controller('MyCtrl', ['$scope', function ($scope) {
  	$scope.page = {
  		active: null,
  		notificator: function (data, type, time) {
  			if (!time){
  				time = 2000
  			}
  			$.notify(data,{
  				style: 'bootstrap',
  				className: type,
  				showDuration: 150,
  				hideDuration: 100,
  				autoHideDelay: time
  			})  			
  		},
  		tables: {
  			Person: {
  				data_link: 'null'
  			},
  			User: {
  				data_link: 'null'
  			},
  			Position: {
  				data_link: 'null'
  			},
  			Departament: {
  				data_link: 'null'
  			},
  			Company: {
  				data_link: 'null'
  			}
  		},
  		tables_count: 0
  	}
}]);

