function table_ctrl (data) {
	var root = this;
	table_ctrl.prototype.glob = {
		tables: {}
	};
	table_ctrl.prototype.notificator = function (data, type, time) {
  			if (!time){
  				time = 2000
  			}
  			$.notify(data,{
  				style: 'bootstrap',
  				className: type,
  				showDuration: 150,
  				hideDuration: 100,
  				autoHideDelay: time
  			})  			
  		}
  	table_ctrl.prototype.update_local_storage = function (name) {
		var data = JSON.stringify(root.glob.tables[name].data);
		localStorage.setItem('js_table_' + name, data);
	}
	var table_factory = function (data, name) {
		var table_root = this;
		this.name = name;
		this.data = data;
		this.read = function (index) {
			if (index === -1){
				return table_root.data
			} else {
				return table_root.data[index]
			}
		}
		this.create = function (data) {
			table_root.data.push(data);
			root.update_local_storage(table_root.name)
		}
		this.delete = function (index) {
			table_root.data.splice(index, 1);
			root.update_local_storage(table_root.name)
		}
		this.update = function (index, data) {
			table_root.data[index] = data;
			root.update_local_storage(table_root.name)
		}
		this.columns = data.columns;
		// $.each(table_root.data[0], function(index, val) {
		// 	table_root.columns.push(index);
		// });
	}

	table_ctrl.prototype.init = function() {
		root.position = data.position;
		$.each(data.tables, function(index, element){
				function load_data (data) {
					// console.log(data);
					root.table_drawer_factory(element, root.position, '<div id="'+ element +'"></div>', 'create')			
					root.glob.tables[element] = new table_factory(data, element);

					var table = root.glob.tables[element];
					var html = root.table_templater_factory (table.name, table.columns, table.read(-1));
					root.table_drawer_factory(table.name, root.position, html, 'update');
				}

				if (!localStorage.getItem('js_table_' + element)){
					$.ajax({
						url: 'js/json/' + element + '.json',
						type: 'GET',
						dataType: 'json',
					})
					.done(function(data) {
						$.ajax({
							url: 'js/json/' + element + '_table.json',
							type: 'GET',
							dataType: 'json',
						})
						.done(function (columns) {
							// console.log(columns);
							localStorage.setItem('js_table_config_' + element, JSON.stringify(columns))
							data.columns = columns;
							load_data(data);
							root.update_local_storage(element)
						})
					})
					.fail(function() {
						console.log('ошибка при загрузке данных в таблицу');
					})
					.always(function() {
					});					
				} else {
					var data = JSON.parse(localStorage.getItem('js_table_' + element));
					var columns = JSON.parse(localStorage.getItem('js_table_config_' + element));
					data.columns = columns;
					load_data(data);
				}
			
		});
	};


	table_ctrl.prototype.table_templater_factory = function (name, columns, data, state) {
		buf = [];
		buf.push(
			'<div id="', name, '" class="block ',(state === 'open')? null : 'little-block' ,' created-table">',
			'<button class="btn btn-primary" style="display: ',(state === 'open')? 'block': 'none' ,'" id="back-but">',
			'	Назад, к списку таблиц',
			'</button>',
			'<div class="shadow" id="shadow" style="display: ',(state === 'open')? 'none': 'block' ,'">',
				'Таблица', name,
			'</div>',
			'<table class="table table-striped table-bordered">',
			'<tr>'
		);
			// console.log(columns);
			$.each(columns.columns, function(index, val) {
				buf.push('<td>');
				buf.push(val.name);
				buf.push('</td>');
			});
			// columns.forEach(function(element, index){
			// });
			buf.push(
			'	<td>',
			'		редактировать/удалить',
			'	</td>',
			'<td>',
			'	открыть подробнее (открывает внешние таблицы)',
			'</td>',
			'</tr>')
			data.forEach(function(element, index){
				buf.push('<tr>');
				$.each(element, function(index, val) {
					buf.push('<td>');
					buf.push(val);
					buf.push('</td>')
				});
				buf.push(
				'	<td>',
				'		<button class="btn btn-primary btn-xs" id="button-change-row" row-no="',index,'" table="',name ,'">',
				'			<span class="glyphicon glyphicon-pencil"></span>',
				'			редактировать',
				'		</button>',
				'		<button class="btn btn-primary btn-xs" id="button-delete-row" row-no="',index,'">',
				'			<span class="glyphicon glyphicon-remove"></span>',
				'			удалить',
				'		</button>',
				'	</td>',
				'<td>',
				'<button class="btn btn-primary btn-xs" id="read-more" key="', index,'" table="', name,'">',
				'	<span class="glyphicon glyphicon-eye-open"></span>',
				'	открыть подробнее',
				'</button>',
				'</td>'
				)
				buf.push('</tr>');
			});
			buf.push('</table>')
			buf.push(
				'<button class="btn btn-primary btn-sm" id="button-add-row">',
				'<span class="glyphicon glyphicon-plus"></span>Добавить новую запись в ',
				name,
				'</button>'
			)
			buf.push(
			'<div class="bg-danger" id="create-row" style="display:none">',
			'<h2 class="text-warning">Добавьте новую запись в ', name ,', введя необходимые данные:</h2 >',
			'<form id="create-form" action="" table=',name ,' method="POST">',
			'	<table class="table table-stripped table-bordered">'
			);
			// console.log(columns.columns);
			$.each(columns.columns, function(index, val) {
				// console.log(val);
				buf.push(
					'<tr>',
					'<td>',val.name ,'</td>',
					'<td><input type="', val.type, '" class="form-control" id="',val.name ,'-create" ', val.required,'/></td>',
					'</tr>'
					)
			});
			// columns.forEach(function(element, index){
			// });
			buf.push(
			'		</tr>',
			'	</table>',
			'	<button class="submit pull-right btn btn-primary" id="button-create-row" >Добавить пользователя</button>',
			'</form>',
			'<button class="btn btn-primary" id="close-add-row">отмена</button>',
			'</div>');
			buf.push(
				'<div class="bg-danger" id="change-row" style="display: none">',
				'	<h2 class="text-warning">Введите новые данные: </h2>',
				'	<form method="POST" id="change-form" table="',name ,'">',		
				'		<table class="table table-stripped table-bordered">')
				$.each(columns.columns, function(index, val) {
				// columns.forEach(function(element, index){
					buf.push(
						'<tr>',
						'<td>',val.name ,'</td>',
						'<td><input type="', val.type, '" class="form-control" id="', val.name,'-update" ', val.required,'/></td>',
						'</tr>'
						)
				});
				buf.push(
				'			</tr>',
				'		</table>',
				'		<button class="submit pull-right btn btn-primary" id="button-update-row" ">редактировать поле</button>',
				'	</form>',
				'	<button class="btn btn-primary" id="close-change-row">отмена</button>',
				'</div>'
				)

			buf.push('</div>')
		return buf.join('');
	}

	table_ctrl.prototype.event_factory = function (obj) {
		obj.on('click', '#shadow', function(event) {
			$('.created-table').hide();
			obj.find('#shadow').hide();
			obj.removeClass('little-block')
			obj.show();
			obj.find('#back-but').show();
		});
		obj.find('#back-but').on('click', function(event) {
			$('.created-table').show();
			obj.find('#shadow').show();
			obj.addClass('little-block')
			obj.find('#back-but').hide();	
		});
		obj.find('#button-add-row').on('click', function(event) {
			obj.find('#create-row').toggle(400);
			obj.find('#change-row').hide(400);
		});
		obj.on('click', '#button-change-row', function (event) {
			obj.find('#change-row').show(400);
			obj.find('#create-row').hide(400);

			var table = event.target.getAttribute('table');
			var columns = root.glob.tables[table].columns;
			var form = root.glob.tables[table].read(event.target.getAttribute('row-no'))
			$.each(columns.columns, function(index, val) {
				obj.find('#' + val.name + '-update').val(form[val.name]);
			});

			obj.attr('change-row', event.target.getAttribute('row-no'))
		})
		obj.on('click', '#close-change-row', function (event) {
			obj.find('#change-row').hide(400);
		})
		obj.on('click', '#close-add-row', function (event) {
			obj.find('#create-row').hide(400);
		})
		obj.on('click', '#button-delete-row', function (event) {
		    root.notificator('Ваша запись обрабатывается сервером...', 'info')
			setTimeout(function () {
				root.glob.tables[obj.attr('id')].delete(event.target.getAttribute('row-no'))
				root.table_drawer_factory(obj.attr('id'), root.position, 'auto', 'update');
		    	root.notificator('Запись успешно удалена', 'success')
			}, 1000)
		})
		
		obj.find('#create-form').submit(function (event) {
			event.preventDefault();
			var table = event.target.getAttribute('table');
			var columns = root.glob.tables[table].columns;
			var form = {}
			$.each(columns.columns, function(index, val) {
				form[val.name] = obj.find('#' + val.name + '-create').val();
			});
			// columns.forEach(function(element, index){
			// });
			root.notificator("Ваша запись обрабатывается сервером...", 'info')
			setTimeout(function () {
				root.glob.tables[table].create(form);
				root.table_drawer_factory(table, root.position, 'auto', 'update');
				root.notificator("Запись успешно добавлена", 'success');	
			}, 1000)
			return false
		})

		obj.find('#change-form').submit(function (event) {
			event.preventDefault();
			var table = event.target.getAttribute('table');
			var columns = root.glob.tables[table].columns;
			var form = {}
			$.each(columns.columns, function(index, val) {
				form[val.name] = obj.find('#' + val.name + '-update').val();
			});
			root.notificator("Ваша запись обрабатывается сервером...", 'info')
			setTimeout(function () {
				root.glob.tables[table].update(obj.attr('change-row'), form);
				root.table_drawer_factory(table, root.position, 'auto', 'update');
				root.notificator("Запись успешно обновлена", 'success');	
			}, 1000)
			
		})

		obj.on('click', '#read-more', function (event) {
			// console.log(event.target.getAttribute('key'));
			var key = event.target.getAttribute('key');
			var table = event.target.getAttribute('table');
			
			$.each(root.glob.tables[table].columns.columns, function(index, val) {
					if (val.ref_key === 'YES'){
						// console.log('find!');
						var column_for_change_name = val.name;
						var ref_table = val.ref_table;
						var column_in_ref_table = val.ref_row;

						$.each(root.glob.tables[table].data[key], function (index, val) {
							if (index === column_for_change_name){
								var key_in_ref_table = val;
								var new_row = root.glob.tables[ref_table].data;
								// console.log(new_row);
								var buf = [];
								$.each(new_row, function (index, val) {
									
									if (val[column_in_ref_table] == key_in_ref_table){
										var buf = '';
										$.each(val, function (index, val) {
											// console.log(index, val);
											if (index!= '$$hashKey'){		
												buf += index + ' : ' + val + '\n';
											}
										})

										root.notificator(buf, 'info', 50000)
									}
								})
							}
						})
						
					}
				})
		})
	}

	table_ctrl.prototype.table_drawer_factory = function (id, parent, html, action) {
		switch(action){
			case 'create':
				parent.append(html)
				break;
			case 'update':
				if (html === 'auto'){
					html = root.table_templater_factory(id, root.glob.tables[id].columns, root.glob.tables[id].data, 'open')
				}
				parent.children('#' + id).replaceWith(html);
				root.event_factory(parent.children('#' + id));
				break;
			default:
				return
				break;
		}
	}
	
	this.init();
}


$( document ).ready(function() {
	var tables = new table_ctrl({
		position: $('div#tables-container'),
		tables: ['Person', 'User', 'Company', 'Position', 'Departament'],
	})
});